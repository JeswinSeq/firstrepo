# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from .models import Post
from django.views.generic import TemplateView,CreateView
from .forms import PostAddForm
# Create your views here.
def post_list(request):
	post=Post.objects.all().order_by('-id').filter(published_date__isnull=False)
	return render(request, 'blog/post_list.html', {'post':post})

def post_pro(request,pk):
	postpro=Post.objects.get(pk=pk)
	return render(request, 'blog/post_list.html',{'postpro':postpro})

def post_sort(request,kid):
	if kid:
		if str(kid)=='pid':
			post=Post.objects.all().filter(published_date__isnull=False).order_by('-published_date')
			print(post)
			return render(request, 'blog/post_list.html',{'post':post})
		elif str(kid)=='cid':
			return post_list(request)

def post_add(request):
	if request.method=='GET':
		form=PostAddForm()
		return render(request,'blog/postadd.html',{'form':form})
	if request.method=='POST':
		form=PostAddForm(request.POST,request.FILES)
		if form.is_valid():
			form.save()
			return post_list(request)
class basehome(TemplateView):
	template_name="blog/base.html"
