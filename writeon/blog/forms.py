from django import forms

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm,UserChangeForm
from .models import Post


class PostAddForm(forms.ModelForm):
	class Meta:
		model=Post
		exclude=['published_date','created_date']
